package it.unimi.di.sweng.lab06.documenti;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FlyweightDocs {

	private int docCounter;
	private HashMap<Integer, String> docs;
	
	public FlyweightDocs(){
		
		docCounter = 0;
		docs = new HashMap<Integer, String>();
	}

	public void creaDocumento(String documento) {
		docs.put(docCounter, documento);
		docCounter += 1;
	}

	public Indice getIndice(){
		
		ArrayList<String> parole = new ArrayList<String>();
		ArrayList<String> contesti = new ArrayList<String>();
		ArrayList<Integer> numeriDoc = new ArrayList<Integer>();
		
		for (int k = 0; k < docs.size(); k++){
			String testo = docs.get(k);
			String[] splitted = testo.split("[ ]");
			StringBuilder parola = new StringBuilder();

			estraiComponenti(parole, contesti, numeriDoc, k, splitted, parola);
		}
		return new Indice(parole, contesti, numeriDoc);
	}

	private void estraiComponenti(List<String> parole, List<String> contesti, List<Integer> numeriDoc,
			int k, String[] splitted, StringBuilder parola) {
		
		for (int i = 0; i < splitted.length;i++){
			StringBuilder contesto = new StringBuilder();
			generaContesto(splitted, i, contesto);
			
			parole.add(parola.toString().trim());
			contesti.add(contesto.toString().trim());
			numeriDoc.add(k);
			parola.append(splitted[i] + " ");
		}
	}

	private void generaContesto(String[] splitted, int i, StringBuilder contesto) {
		for (int j = i; j < splitted.length; j++)
			contesto.append(splitted[j] + " ");
	}
}
