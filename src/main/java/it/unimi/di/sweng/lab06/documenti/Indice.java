package it.unimi.di.sweng.lab06.documenti;

import java.util.List;

public class Indice {
	public final List<String> parole;
	public final List<String> contesti;
	public final List<Integer> numeriDoc;
	
	Indice(List<String> parole, List<String> contesti, List<Integer> numeriDoc){
		
		this.parole = parole;
		this.contesti = contesti;
		this.numeriDoc = numeriDoc;
	}
}
