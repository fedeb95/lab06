package it.unimi.di.sweng.lab06.input;

import java.io.Reader;
import java.util.List;

public abstract class AbstractFactLettore {
	
	public abstract AbstractLettore creaLettoreInput(Reader r);
	
	public abstract AbstractLettore creaLettoreNoPunt(Reader r);
	
	public abstract AbstractLettore creaLettoreStopWords(Reader r, List<String> stopWords);
}
