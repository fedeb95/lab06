package it.unimi.di.sweng.lab06.input;

import java.io.Reader;
import java.util.List;

public class LettoreFactory extends AbstractFactLettore {

	@Override
	public AbstractLettore creaLettoreInput(Reader r) {
		return new LettoreInput(r);
	}

	@Override
	public AbstractLettore creaLettoreNoPunt(Reader r) {
		return new LettoreNoPunt(r);
	}

	@Override
	public AbstractLettore creaLettoreStopWords(Reader r, List<String> stopWords) {
		return new LettoreStopWords(r, stopWords);
	}

}
