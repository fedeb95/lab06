package it.unimi.di.sweng.lab06.input;

import java.io.Reader;
import java.util.Scanner;

public class LettoreInput extends AbstractLettore {

	private Scanner scan;
	
	public LettoreInput(Reader r) {
		scan = new Scanner(r);
		scan.useDelimiter("\n");
	}

	@Override
	public boolean hasNext() {

		return scan.hasNext();
	}

	@Override
	public String next() {
		
		return scan.next();
	}

}
