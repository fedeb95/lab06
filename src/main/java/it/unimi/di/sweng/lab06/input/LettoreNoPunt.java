package it.unimi.di.sweng.lab06.input;

import java.io.Reader;
import java.util.Scanner;

public class LettoreNoPunt extends AbstractLettore {

	private Scanner scan;
	
	public LettoreNoPunt(Reader r){
		scan = new Scanner(r);
		scan.useDelimiter("\n");
	}
	
	@Override
	public boolean hasNext() {
		
		return scan.hasNext();
	}

	@Override
	public String next() {
		String next = scan.next();
		String[] splitted = next.split("[^a-zA-z ]");
		StringBuilder sb = new StringBuilder();
		for(String substr : splitted)
			sb.append(substr);
		return sb.toString();
	}

}
