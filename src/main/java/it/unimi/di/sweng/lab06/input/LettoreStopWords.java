package it.unimi.di.sweng.lab06.input;

import java.io.Reader;
import java.util.List;
import java.util.Scanner;

public class LettoreStopWords extends AbstractLettore {

	Scanner scan;
	List<String> stopWords;
	
	public LettoreStopWords(Reader r, List<String> stopWords) {

		this.scan = new Scanner(r);
		scan.useDelimiter("\n");
		this.stopWords = stopWords;
	}
	
	@Override
	public boolean hasNext() {
		return scan.hasNext();
	}

	@Override
	public String next() {
		String next = scan.next();
		String[] splitted = next.split("[ ]");
		StringBuilder sb = new StringBuilder();
		for (String s : splitted)
			checkAppend(sb, s);
		return sb.toString().trim();
	}

	private void checkAppend(StringBuilder sb, String s) {
		if (!stopWords.contains(s))
			sb.append(s).append(" ");
	}

}
