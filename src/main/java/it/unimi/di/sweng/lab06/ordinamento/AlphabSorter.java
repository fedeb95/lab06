package it.unimi.di.sweng.lab06.ordinamento;

import java.util.List;

import it.unimi.di.sweng.lab06.documenti.Indice;

public class AlphabSorter extends Sorter {

	@Override
	public void sort(Indice indice) {
		
		List<String> parole = indice.parole;
		List<String> contesti = indice.contesti;
		List<Integer> numeriDoc = indice.numeriDoc;
		for (int i = 1; i < contesti.size(); i++){
			int j = i-1;
			controllaPrecedenti(parole, contesti, numeriDoc, j);
		}
	}

	private void controllaPrecedenti(List<String> parole, List<String> contesti, List<Integer> numeriDoc, int j) {
		String tmpParola;
		String tmpContesto;
		Integer tmpNumero;
		while (j >= 0 && contesti.get(j+1).compareTo(contesti.get(j)) < 0){
			tmpContesto = contesti.set(j+1, contesti.get(j));
			contesti.set(j, tmpContesto);
			tmpParola = parole.set(j+1, parole.get(j));
			parole.set(j, tmpParola);
			tmpNumero = numeriDoc.set(j+1, numeriDoc.get(j));
			numeriDoc.set(j, tmpNumero);
			j--;
		}
	}

}
