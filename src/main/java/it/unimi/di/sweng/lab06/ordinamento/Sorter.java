package it.unimi.di.sweng.lab06.ordinamento;

import it.unimi.di.sweng.lab06.documenti.Indice;

public abstract class Sorter {
	
	public abstract void sort(Indice indice);
}
