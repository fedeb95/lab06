package it.unimi.di.sweng.lab06.documenti;

import static org.junit.Assert.*;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import it.unimi.di.sweng.lab06.documenti.FlyweightDocs;
import it.unimi.di.sweng.lab06.documenti.Indice;
import it.unimi.di.sweng.lab06.input.AbstractFactLettore;
import it.unimi.di.sweng.lab06.input.AbstractLettore;
import it.unimi.di.sweng.lab06.input.LettoreFactory;
import it.unimi.di.sweng.lab06.input.TestInput;

public class TestIndice {
	
	@Rule
	public Timeout globalTimeout = Timeout.seconds(2); // 2 seconds max per test
	
	@Test
	public void testFlyweightDocs(){
		
		TestInput.redirectInput("Maramao perche sei morto");
		Reader r = new InputStreamReader(System.in);
		
		AbstractFactLettore fact = new LettoreFactory();
		AbstractLettore lettore = fact.creaLettoreInput(r);
		
		FlyweightDocs documenti = new FlyweightDocs();
		while(lettore.hasNext())
			documenti.creaDocumento(lettore.next());
		
		Indice indice = documenti.getIndice();
		
		List<String> testParole = new ArrayList<String>();
		List<String> testContesti = new ArrayList<String>();
		List<Integer> testNumeri = new ArrayList<Integer>();
		testParole.add("");
		testContesti.add("Maramao perche sei morto");
		testParole.add("Maramao");
		testContesti.add("perche sei morto");
		testParole.add("Maramao perche");
		testContesti.add("sei morto");
		testParole.add("Maramao perche sei");
		testContesti.add("morto");
		for (String p : testParole)
			testNumeri.add(0);
		
		for (int i = 0; i < testParole.size(); i++){
			assertEquals(testParole.get(i), indice.parole.get(i));
			assertEquals(testContesti.get(i), indice.contesti.get(i));
			assertEquals(testNumeri.get(i), indice.numeriDoc.get(i));
		}
	}
}
