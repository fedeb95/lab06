package it.unimi.di.sweng.lab06.input;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import it.unimi.di.sweng.lab06.input.AbstractFactLettore;
import it.unimi.di.sweng.lab06.input.AbstractLettore;
import it.unimi.di.sweng.lab06.input.LettoreInput;
import it.unimi.di.sweng.lab06.input.LettoreNoPunt;
import it.unimi.di.sweng.lab06.input.LettoreStopWords;

public class TestInput {
	
	@Rule
	public Timeout globalTimeout = Timeout.seconds(2); // 2 seconds max per test
	
	@Test
	public void testLettura(){
		
		redirectInput("Maramao perche sei morto\nPan e vin non ti mancava");
		Reader r = new InputStreamReader(System.in);
		AbstractLettore lettore = new LettoreInput(r);
		
		assertEquals("Maramao perche sei morto", lettore.next());
		assertEquals(true, lettore.hasNext());
		assertEquals("Pan e vin non ti mancava", lettore.next());
	}

	@Test
	public void testLetturaNoPunteggiatura(){
		
		redirectInput("Maramao, perche sei morto?\nPan, e vin, non ti mancava!");
		Reader r = new InputStreamReader(System.in);
		AbstractLettore lettore = new LettoreNoPunt(r);
		
		assertEquals("Maramao perche sei morto", lettore.next());
		assertEquals(true, lettore.hasNext());
		assertEquals("Pan e vin non ti mancava", lettore.next());
		
	}
	
	@Test
	public void testLetturaStopWords(){
		
		redirectInput("Maramao perche sei morto\nPan e vin non ti mancava");
		Reader r = new InputStreamReader(System.in);
		List<String> stopWords = new ArrayList<String>();
		stopWords.add("e");
		AbstractLettore lettore = new LettoreStopWords(r, stopWords);
		
		assertEquals("Maramao perche sei morto", lettore.next());
		assertEquals(true, lettore.hasNext());
		assertEquals("Pan vin non ti mancava", lettore.next());
		
	}
	
	@Test 
	public void testLetturaFactory(){
		
		AbstractFactLettore fact = new LettoreFactory();
		
		redirectInput("Maramao perche sei morto");
		Reader r = new InputStreamReader(System.in);
		AbstractLettore lettoreSemplice = fact.creaLettoreInput(r);
		assertEquals("Maramao perche sei morto", lettoreSemplice.next());
		
		
		redirectInput("Maramao, perche sei morto?");
		r = new InputStreamReader(System.in);
		AbstractLettore lettoreNoPunt = fact.creaLettoreNoPunt(r);
		assertEquals("Maramao perche sei morto", lettoreNoPunt.next());
		
		List<String> stopWords = new ArrayList<String>();
		stopWords.add("e");
		redirectInput("Pan e vin non ti mancava");
		r = new InputStreamReader(System.in);
		AbstractLettore lettoreStopWords = fact.creaLettoreStopWords(r, stopWords);
		assertEquals("Pan vin non ti mancava", lettoreStopWords.next());
	}
	
	public static void redirectInput(String s) {
		String userInput = s;
		ByteArrayInputStream in = new ByteArrayInputStream(userInput.getBytes());
		System.setIn(in);
	}
}
