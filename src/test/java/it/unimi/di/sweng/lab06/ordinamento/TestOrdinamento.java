package it.unimi.di.sweng.lab06.ordinamento;

import static org.junit.Assert.assertEquals;

import java.io.InputStreamReader;
import java.io.Reader;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import it.unimi.di.sweng.lab06.documenti.FlyweightDocs;
import it.unimi.di.sweng.lab06.documenti.Indice;
import it.unimi.di.sweng.lab06.input.AbstractFactLettore;
import it.unimi.di.sweng.lab06.input.AbstractLettore;
import it.unimi.di.sweng.lab06.input.LettoreFactory;
import it.unimi.di.sweng.lab06.input.TestInput;
import it.unimi.di.sweng.lab06.ordinamento.AlphabSorter;
import it.unimi.di.sweng.lab06.ordinamento.Sorter;

public class TestOrdinamento {
	
	@Rule
	public Timeout globalTimeout = Timeout.seconds(2); // 2 seconds max per test
	
	@Test
	public void testOrdineAlfabetico(){
		
		TestInput.redirectInput("Maramao perche sei morto");
		Reader r = new InputStreamReader(System.in);
		
		AbstractFactLettore fact = new LettoreFactory();
		AbstractLettore lettore = fact.creaLettoreInput(r);
		
		FlyweightDocs documenti = new FlyweightDocs();
		while(lettore.hasNext())
			documenti.creaDocumento(lettore.next());
		
		Indice indice = documenti.getIndice();
		
		Sorter sr = new AlphabSorter();
		sr.sort(indice);
		
		for(int i = 1; i < indice.contesti.size(); i++){
			
			String contestoCorrente = indice.contesti.get(i);
			String contestoPrecedente = indice.contesti.get(i-1);
			assertEquals(true, contestoCorrente.compareTo(contestoPrecedente) >= 0);
		}
	}
}
